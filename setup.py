#!/usr/bin/env python
from setuptools import setup

setup(
    name="hang_socket_server",
    version="1.1.2",
    author="5ive",
    description="Hang malicious IPs and track",
    packages=["hang_socket_server"],
    install_requires=[],
    entry_points={
        "console_scripts": [
            "hang-socket-server=hang_socket_server.hang_socket_server:main"
        ]
    },
)
