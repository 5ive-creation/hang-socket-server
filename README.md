Create service user
adduser --force-badname --system --home /var/cache/hang-socket-server --quiet _hang-socket-server

Kill service with
touch kill

Restart service with
touch restart
