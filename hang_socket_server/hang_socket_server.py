#!/usr/bin/python3 -u

import re
import os
import sys
import socket
import signal
import json
import argparse
import configparser
from threading import Thread

# from psutil import virtual_memory # sudo apt install python3-psutil
from time import sleep


class Application:
    ips = {}
    json_file = os.path.join("/var/run/user/", str(os.getuid()), "ips.json")
    ignore_list_regex = None

    def on_new_client(self, clientsocket, addr):
        #        try:
        #            machine = list( socket.gethostbyaddr( addr[0] ) )
        #            machine.append( addr[1] )
        #        except Exception:
        if self.ignore_list_regex and self.ignore_list_regex.match(str(addr[0])):
            print("INFO: Ignoring allowed connection from: " + str(addr))
            clientsocket.close()
            return True
        self.ips[addr[0]] = addr[0]
        print("INFO: Connection from: {}".format(addr), flush=True)
        while not self.kill and clientsocket.fileno() != -1:
            sleep(1)

    def bind_to_port(self, socket, port):
        print("INFO: Attemping bind to port " + str(port) + "...")
        try:
            socket.bind((self.HOST, port))
        except Exception:
            return False
        return True

    def signal_handler(self):
        print()
        print("INFO: Shutting down server...")
        self.kill = True

    def kill_old_connection(self, amount, connections):
        if len(connections) > 1:
            message = " connections. closing oldest connections first"
        else:
            message = " connection."
        print("INFO: " + str(len(connections)) + message)
        if amount > len(connections):
            amount = len(connections)
        for i in range(0, amount):
            if len(connections) > 0:
                print("INFO: Killing: " + str(amount - i) + " remaining")
                connections[0].close()
                del connections[0]

    def flush_ips_to_disk(self):
        print("INFO: Writing collected IPs to disk")
        clear_ips = not os.path.exists(self.json_file)
        with open(self.json_file, "w") as file:
            json.dump(self.ips, file, sort_keys=True)
        if clear_ips:
            print("INFO: Clearing IPs from memory")
            self.ips = {}

    def read_config(self, path):
        config = configparser.ConfigParser()
        path = os.path.normpath(path)
        if os.path.exists(path):
            config.read(path)
        return config

    def __init__(self):
        self.HOST = ""  # Symbolic name meaning all available interfaces
        THRESHOLD = 10 * 1024 * 1024  # 10MB
        SLEEP = 10
        self.kill = False

        parser = argparse.ArgumentParser(
            description="Create a server that responds with a near endless sleep connection",
            formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        )
        parser.add_argument(
            "-p",
            "--port",
            help="Port to start listening on",
            type=int,
            default=5000,
        )
        parser.add_argument(
            "--max-connections",
            help="Specify maximum amount of connections",
            type=int,
            default=10,
        )
        parser.add_argument(
            "-a",
            "--purge-amount",
            help="Number of connections to purge when maximum connection limit is reached",
            type=int,
            default=9,
        )
        parser.add_argument(
            "-f",
            "--json-dump",
            help="JSON file to dump ips collected",
            default=self.json_file,
        )
        parser.add_argument(
            "-c",
            "--config",
            help="Configuration file",
            default="/etc/hang-socket-server/hang-socket-server.conf",
        )
        parser.add_argument(
            "-v", "--verbose", help="More console output", action="store_true"
        )
        args = parser.parse_args()

        self.json_file = args.json_dump
        self.verbose = args.verbose

        if self.verbose:
            print("INFO: config file located at: " + os.path.realpath(args.config))
        if self.verbose:
            print(
                "INFO: Script executing from path: "
                + os.path.dirname(os.path.realpath(__file__))
            )
        if self.verbose:
            print("INFO: Executing script: " + os.path.realpath(__file__))
        if self.verbose:
            print("INFO: IPs dumped to: " + os.path.realpath(self.json_file))

        config = self.read_config(args.config)
        self.ignore_list_regex = re.compile(
            config["DEFAULT"].get("ignore_ips_regex", "no configuration variable")
        )

        if os.path.exists(self.json_file):
            with open(self.json_file, "r") as file:
                self.ips = json.load(file)

        signal.signal(signal.SIGINT, lambda signal, frame: self.signal_handler())
        signal.signal(signal.SIGTERM, lambda signal, frame: self.signal_handler())

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            #            mem = virtual_memory()
            connections = []

            while not self.kill and not self.bind_to_port(s, args.port):
                print("WARNING: Unable to bind, sleeping: " + str(int(SLEEP)) + " secs")
                sleep(SLEEP)
                SLEEP = SLEEP + (SLEEP % 0.1)

            if not self.kill:
                print(
                    "INFO: Server started (max connections: "
                    + str(args.max_connections)
                    + ")"
                )
                if self.verbose:
                    print("Waiting for clients...")
                s.listen(1)

                while not self.kill:
                    try:
                        conn, addr = s.accept()  # Establish connection with client.
                        connections.append(conn)
                        Thread(target=self.on_new_client, args=(conn, addr)).start()

                    except OSError as ose:
                        print("Error: " + str(OSError))
                        self.kill_old_connection(args.purge_amount, connections)
                        self.kill = True
                    except RuntimeError as rte:
                        print("Error: " + str(rte))
                        self.kill_old_connection(args.purge_amount, connections)
                        self.kill = True
                    except Exception as e:
                        print("Error: " + str(type(e)))
                        if hasattr(e, "message"):
                            print(str(e.message))
                        else:
                            print(str(e))
                        self.kill_old_connection(args.purge_amount, connections)

                    #                    if mem.free < THRESHOLD:
                    #                        print( 'WARNING: LOW MEM ' + str( mem.free ) )
                    #                        self.kill_old_connection( args.purge_amount , connections )

                    if len(connections) > args.max_connections:
                        print("INFO: Exceeded limit to connections")
                        self.kill_old_connection(args.purge_amount, connections)
                        self.flush_ips_to_disk()
                    else:
                        print(
                            "INFO: "
                            + str(args.max_connections - len(connections))
                            + " of max connections remaining"
                        )

        if len(connections) > 0:
            print("INFO: Killing existing connections...")
            self.kill_old_connection(len(connections), connections)

        self.flush_ips_to_disk()


def main():
    app = Application()


if __name__ == "__main__":
    main()
